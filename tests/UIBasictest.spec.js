const {test, expect} = require('@playwright/test');

test.only('Browser Context Playwright test',async ({browser})=>
{
    //chrome - plugins/cookies
    const context = await browser.newContext();
    const page = await context.newPage();
    const userName = page.locator('#username');
    const signIn = page.locator("#signInBtn");
    const cardTitles = page.locator(".card-body >> a");
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    console.log (await page.title())
    //CSS, Xpath Selectors
    await userName.type('rahulshetty');
    await page.locator("[type='password']").type('learning');
    await signIn.click();
    // In Selenium we need to use wait until this locator shown on the page.
    // In Playwright there is an intelligent wait to wait for the web element
    console.log(await page.locator("[style*='block']").textContent());
    await expect(page.locator("[style*='block']")).toContainText('Incorrect');
    //type  and fill
    await userName.fill("");
    // if we enter the blank value to the text box.
    // it will erase the old value/text and add the new text.
    await userName.fill("rahulshettyacademy");
    //await page.waitForNavigation()
    // There will be a event soon,
    // wait for the page to be navigated fully.

    //Race condition
    await Promise.all(
        [
            page.waitForNavigation(),
            signIn.click(),
        ]
    );
    
    // console.log(await cardTitles.nth(1).textContent());
    // console.log(await cardTitles.first().textContent());
    
    const allTitles = await cardTitles.allTextContents();
    console.log(allTitles);
    
});

test('Page Playwright test',async ({page})=>
{
    // //chrome - plugins/cookies
    // const context = await browser.newContext();
    // const page = await context.newPage();
    await page.goto("https://www.google.com/");
    //get the title - Assertion
    console.log (await page.title())
    await expect(page).toHaveTitle("Google");
    
});