# Playwright JS Automation Testing from Scratch with Framework
# Playwright -Modern Testing tool for reliable Web Automation. Support API calls with in UI tests for Smarter Automation.

# What you'll learn
* Understand the Web Automation with Playwright on Live Applications
Comprehensive knowledge on all Playwright Methods and their usage with examples
* Learn How to make API calls with in UI Automation tests to perform smart automation
* Understand how to Intercept Network/API requests & responses with Playwright
* Get Familiar with Playwright Inspector tool, Trace Viewer & Code Gen (Record Playback) tools
* Develop End to end Framework with Playwright utilizing all the features available in the tool
* Learn Visual testing with Playwright to compare the Images
* Learn JavaScript fundamentals required for Playwright Automation testing

# Course content:
* Introduction to Playwright Automation and course expectations
* Getting started with Playwright Automation Core Concepts
* Playwright Basic methods for Web Automation testing with examples
* Handling UI components(Dropdowns, Radio Buttons, Childwindows) with playwright
* Learn Playwright Inspectors, trace viewers and codegen tools with Demo Examples.
* End to End Web automation Practise Exercise with playwright
* Handling web dialogs, frames and Event Listeners with Playwright.
* API testing with playwright and Build mix of Web & API Tests
* Session storage and Intercepting Network request/responses with playwright
* Perform visual testing with playwright Algorithms
* Page object Patterns and Data Driven Parameterization for playwright Tests
* Project configuration and config options for robust Framework Design
* Test Retries, serial and parallel execution and tagging tests in playwright
* HTML and Allure reporting and CI/CD Jenkins

# Course BreakDown:
* Playwright Web/UI Automation (covers all core concepts) – 50%
* Mix of Web & API Tests including Network mocking. – 25%
* Playwright Advanced Framework features. – 25%
* JavaScript Basics for Beginners (optional)

# Section 1: * Introduction to Playwright Automation and course expectations

# Why Playwright ? why is it a HOT cake in the market?
* Playwright enables reliable end to end testing for modern web apps with its Auto-wait capability
* Works on major browser which uses chromium engine (Chrome and Edge), Firefox, safari(Web Kit) and opera.
* Works on any OS - windows, mac and linux and support native mobile emulation of google chrome for android and Mobile Ios in safari.
* Works with any language - JavaScript , Type script, Java, Python, C#
* Playwright have execellent inbuilt features called traces which can take automatic screen shots, test video recording, flaky test retry and logging Mechanism.
* Playwright provides inspector tool which help us to monitor and debug every steps of execution, see click points and verify page locators on fly.
* Playwright has inbuilt API testing libaries to fire the network calls on fly within web application (Test Edge case scenarios with Mix of web and API testing)
* Playwright provides browser contect features which help to save and transfer the browser state to any othe new browser.
* Playwright provides codegen tool which generate test code by recording your actions. Save them into any language.

# NPM Trends for JavaScript Playwright.

# what is node.js ?
* Node.js is an open source cross platform back end javascript runtime environment that runs on the V8 engine and executes Javascript code outside a web browser.

* Install Node.js

* Install Visual Studio Code (Best for Javascript/TypeScript)

* Playwright official website - https://playwright.dev/

Install Playwright dependencies-

* npm init playwright

# Create npm project and install Playwright Dependencies for testing

* There is one command which will install the Playwright and necessary dependencies for testing.

$ npm init playwright
# We are creating a new node project which is called playwright. It will install all the playwright dependencies.

# Do you want to use TypeScript or JavaScript : Javascript
# where to put your end-to-end tests?: tests
# Add a GitHub Actions workflow? Yes
# creating a node project 
# this will create a project in the playwright structure.
# Make sure to install node.js and also make sure node.js path is mentioned in the system path.

# playwright.config.js -> This is the test runner for my entire test cases.

# package.json: Default file gets created for every node project. Any JavaScript Project, Package.json is a must.

# package-lock.json is a ignorable file for us.

# Playwright created a folder names tests, which has the sample test case in it. which is around 30 cases in it. 

# node_modules: Has all the necessary jars needed for the playwright module. If we delete this, there is no playwright in this project.

# test folder and playwright.config.js is the main folder in the project. This test folder is responsible for the UI automation, API Automation and network responses.

# Importance of Playwright Test Annotations and async await understanding:


// We need to import a playwright annotation from playwright module.
// All my playwright modules are installed in my node modules.They are nothing but Playwright JARS.

const {test} = require('@playwright/test');

//{test}: These tests are executed in Playwright environment that launches the browser and provides a fresh page to each test.


test('First Playwright test',function()
{
// test annotation takes two arguments, first argument is the test case name, and the second argument is test function. 
Inside the second argument is where we write the actual code, And this completely treated as one test case.

});
{
// playwright code - 
//step 1
//step 2
//step 3
// There is no gaurantee that these steps will execute in sequence steps
// In Javascript all the 3 steps will try to execute at the same time.
// That is what we call as asynchronous.
// We have to explicit say dont go to next step unless the current step is completed.
// for that we have to use the key word await for every step.
// If we are using the keyword await in our script we have to make sure the function is tagged with keyword async

test('First Playwright test',async function())
await gets activated only when we use the async keyword in our function.


// In the new version of Javascript if the function does not have a name we call it as annoymous function
// for annoymous function we can skip using the keyword function and replace it with a fat pipe.

test('First playwright test', async ()=>
)

// there is a fixture that is coming called browser: This fixture comes by default from playwright module.
test('Browser Context Playwright test',async ({browser})=>

// Fixtures are nothing but global variables which are available across the projects.

// The browser fixture is globally present to all the tests.
// There are totally 4 fixtures present in playwright

// we need to pass the browser as a parameter to the test functions first. And that parameter will passed to the function body.

// To represent an represent this browser to be playwright feature we need to represent the browser inside the curly braces.

// we need to create a new context here: What is the context here?
// chrome - plugins/ cookie

});

test('First Playwright test', async ({browser}))=>
{
  //chrome -plugins/cookies
  
  const context = await browser.newContext()
  //Creating a new instances in the browser.
  
  const page =  await context.newPage()
  //creating tab in the browser to launch the page.
}

const config = {
  testDir: './tests',
  /* Maximum time one test can run for. */
  timeout: 30 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000
  },

  # Here we are creating a object called config.
  # Let us see what are the properties present in the object.
  
  # By default the playwright runs in headless mode.
  # we need to explicitly say run in head mode.


#Commands to run the test script:

$ npx playwright test
runs the end-to-end tests.

$ npx playwright test --project=chromium
runs the tests only on Desktop Chrome

$ npx playwright test tests/example.spec.js
runs the tests of a specific file

$ npx playwright test --debug
runs the tests in debug mode

Note: 
npx is the path which will point to the path of our playwright module in the node module 

Remember:
Test cases present in the same file will run in sequential order
Multiple test files, then those files will be executed parallely.

In the terminal pass the below command for execution:
$  npx playwright test
The above command runs in the headless mode.

If we want to run the script in headed mode:
$ npx playwright test --headed

If we want to run a selected test case, then name that test case as only.


test.only('Page Playwright test',async ({page})=>
{
    // //chrome - plugins/cookies
    // const context = await browser.newContext();
    // const page = await context.newPage();
    await page.goto("https://www.google.com/");
    
});

# Section 3 Playwright Basic methods for Web Automation testing with examples:

# Locators supported by playwright and how to type into elements on page:

From playwright.config.js:
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000
  },

  It will wait for 5 seconds for the web element to be visible.

  The below step will wait max for 5 seconds
     await expect(page).toHaveTitle("Google");

Note: The default time out for all assertion is 5 seconds.

And the timeout for over all test is 30 seconds.
Here 30 seconds is the global timeout and 5 seconds is for the assertion time out.

Playwright pre-dominatly supports CSS selector
It is always good to use CSS selector in playwright automation.

# Rules for CSS Selectors:
If Id is present 
CSS -> tagname#id (or) #id

If class attribute is present
CSS -> tagname.class (or) .class

write CSS based on any Attribute
CSS -> [attribute='value']

write CSS with traversing from parent to child
CSS -> parenttagname >> childtagname

If needs to write the locator based on text
text=''

# Extracting the text from browser and inserting valid expect assertions in test

# How to work with locators which extract multiple webelements in page

* Playwright has two ways to enter the text in the text box.
1. using type method
2. using fill method
 
 fill method will clear the existing text from the text box and fill the text box with new value.

# Understanding how wait mechanism works if list of elements are returned.

The method : allTextContents()
will not wait for the element to be displayed on the page.

where as textContent() 
will wait for the element to be displayed on the page.

# Techniques to wait dynamically for new page in service based applications.

url: https://rahulshettyacademy.com/client/dashboard/dash

if we right click page inspect, and move to network and Fetch/XHR
* All the data that is displayed on the web page is displayed by the API calls.

* Here data is loaded by service calls.
* Here it made a call to an end point: https://rahulshettyacademy.com/api/ecom/product/get-all-products

* Data is rendered on the web page through the network calls.
* In the network tab, we can see all network calls.
* And if the company is developing a new  application, it will be based on service based architecture.
* Playwright will wait untill all the calls are made.

Ex:2
Refering the second application:
https://rahulshettyacademy.com/angularpractice/shop

Here in this application, while login to the application no network calls are made.

Hence this is not service oriented app.

Here API calls are not giving the data.

Here the data comes direct from the server.

We know javascript is asynchronous in nature, 
Every step in Playwright Javascript will return a promise object.
To resolve that promise object we use the await keyword.
await keyword resolves the promise object and performs the actions type or click.

